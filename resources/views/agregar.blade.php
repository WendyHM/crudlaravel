@extends('plantilla')

@section('contenido')

<!-- Contenido --> 
<section class="content"> 
    <h1>Agregar Nuevo Curso</h1>
    <!-- Formulario --> 
    <form action="{{ route('store') }}" method="POST">
        @csrf 

        <!-- 1 columna -->
        <div class="form-outline mb-4">
            <input type="text" id="nombre" name="nombre" class="form-control" />
            <label class="form-label" for="nombre">Nombre</label>
        </div>

        <!-- 1 columna -->
        <div class="form-outline mb-4">
            <input type="text" id="objetivo" name="objetivo" class="form-control" />
            <label class="form-label" for="objetivo">Objetivo</label>
        </div>

        <!-- 2 columnas  -->
        <div class="row mb-4">
            <div class="col">
            <div class="form-outline">
                <input type="text" id="modalidad" name="modalidad"  class="form-control" />
                <label class="form-label" for="modalidad">Modalidad</label>
            </div>
            </div>
            <div class="col">
            <div class="form-outline">
                <input type="text" id="cupo" name="cupo"  class="form-control" />
                <label class="form-label" for="cupo">Cupo</label>
            </div>
            </div>
        </div>

        <!-- 2 columnas  -->
        <div class="row mb-4">
            <div class="col">
            <div class="form-outline">
                <input type="text" id="periodo" name="periodo"  class="form-control" />
                <label class="form-label" for="periodo">Periodo</label>
            </div>
            </div>
            <div class="col">
            <div class="form-outline">
                <input type="text" id="horario" name="horario"  class="form-control" />
                <label class="form-label" for="horario">Horario</label>
            </div>
            </div>
        </div>
        
        <!-- 2 columnas  -->
        <div class="row mb-4">
            <div class="col">
            <div class="form-outline">
                <input type="text" id="dias" name="dias"  class="form-control" />
                <label class="form-label" for="dias">Días</label>
            </div>
            </div>
            <div class="col">
            <div class="form-outline">
                <input type="text" id="salon" name="salon"  class="form-control" />
                <label class="form-label" for="salon">Salón</label>
            </div>
            </div>
        </div>

        <!-- Submit button -->
        <button type="submit" class="btn btn-primary btn-block mb-4">Agregar</button>

        
    </form>
</section>

@endsection 